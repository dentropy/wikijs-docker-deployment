# wiki.js public deployment

The goal of this project is to deploy a secure wiki on the public internet for users to interact with.

## Local Testing Tutorial

### Requirements

* Docker
* Bash

### Steps

1. Run start.sh script
2. Navigate to <http://localhost:3232> in your browser
3. Perform wiki.js setup, including adding first page of the wiki
4. Run the ```backup.sh``` script
5. Run ```nuke.sh``` script
6. Test backup by running ```restore.sh``` script
7. Navigate back to <http://localhost:3232> and make sure you can log out and back in

## Public Deployment Tutorial

### Requirements

* Debian based server
  * Public IP address
  * Ports 80 and 443 open
* Docker
* Nginx

### Steps

1. Perform local testing tutorial and set up a backup
2. Forward Domain Name to Server IP Address
3. Confirm domain name is configured correctly

    ```nslookup YOUR_DOMAIN_NAME```

4. SSH into server using domain name to make sure connectivity works

    ```ssh YOUR_DOMAIN_NAME```

5. Open new shell and copy contents to server from your local machine

    ```scp ./wikijs-docker USERNAME@YOUR_DOMAIN_NAME:~```

    * Alternatively you can use ```rsync```, it is faster

    ```rsync ./wikijs-docker USERNAME@YOUR_DOMAIN_NAME:~```

6. In SSH shell update then install Install Nginx and letsencrypt 

    ``` bash
    sudo apt update -y
    sudo apt install -y nginx
    sudo apt install -y letsencrypt
    ```

7. Save your email and domain to environment variables

    ``` bash
    YOUR_DOMAIN_NAME="test.com"
    YOUR_EMAIL_ADDRESS="you@test.com"
    ```
7. Stop nginx in case it started, get SSL Cert from letsencrypt, restart nginx

    ``` bash
    sudo mkdir -p /var/lib/letsencrypt/
    sudo systemctl stop nginx
    sudo certbot certonly --email $YOUR_EMAIL_ADDRESS -d $YOUR_DOMAIN_NAME --standalone
    sudo systemctl start nginx
    ```

8. Configure Nginx proxy into docker container

    ``` bash
    sudo cp ~/wikijs-docker/nginx_config.conf /etc/nginx/sites-available/$YOUR_DOMAIN_NAME
    sudo sed -i 's/YOUR_DOMAIN_NAME/'"$YOUR_DOMAIN_NAME"'/g' /etc/nginx/sites-available/$YOUR_DOMAIN_NAME
    sudo ln -s /etc/nginx/sites-available/$YOUR_DOMAIN_NAME /etc/nginx/sites-enabled/$YOUR_DOMAIN_NAME
    sudo systemctl restart nginx
    ```
    * Troubleshoot config using ```nginx -t```
    * If all has passed restart nginx using ```sudo systemctl restart nginx```


9. Restore wikijs backup by navigating to the wikijs-docker folder and running the backup.sh script

    ``` bash
    cd ~/wikijs-docker && bash restore.sh
    ```

10. Go to your domain name and use the wiki

## Troubleshooting
    
Use ssh port forwarding to make sure the docker container works

``` bash
ssh -L 3232:localhost:3232 YOUR_DOMAIN_NAME
```