#!/bin/bash
mkdir ~/wikijs-docker/backups
echo "Stopping containers for wiki.js"
docker-compose stop
docker run --rm \
  -v wikijs-db:/data \
  -v ~/wikijs-docker/backups:/backup ubuntu bash \
  -c "cd /data && tar cvf /backup/wikijs-db-$(date +"%F-%T").tar ."
echo "Starting containers for wiki.js"
docker-compose start
