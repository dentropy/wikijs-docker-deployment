#!/bin/bash

echo "Stopping all docker containers"
docker stop wikijs-db
docker stop wikijs-server
echo "Removing all docker containers"
docker rm wikijs-db
docker rm wikijs-server
echo "Removing volume"
docker volume rm wikijs-db
