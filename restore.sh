#!/bin/bash
backups="$(ls backups)"
PS3="Enter a number: "

# https://linuxize.com/post/bash-select/

select backup in $backups
do
    echo "Selected backup: $backup"
    echo "do you wish to continoue"
    read -r -p "Are You Sure? [Y/n] " input
 
    case $input in
        [yY][eE][sS]|[yY])
    echo "Restoring $backup"
    break;
    ;;
        [nN][oO]|[nN])
    echo "Please try selecting a backup again"
        ;;
        *)
esac
done

echo "Performing restore"
echo "Stopping containers"
docker-compose stop
echo "Removing previous volume"
docker volume rm wikijs-db
echo "Recreating volume"
docker volume create wikijs-db
echo "Restoring volume state"
docker run --rm \
  -v wikijs-db:/recover \
  -v "$(pwd)/backups:/backup" ubuntu bash \
  -c "cd /recover && tar xvf /backup/$backup"
echo "Restarting containers"
docker-compose up -d
echo "Completed restore of $backup backup"
